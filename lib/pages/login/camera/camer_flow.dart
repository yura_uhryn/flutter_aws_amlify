import 'package:aws_amlify_flutter/storage_service/service.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

import 'camera_page.dart';
import 'galery_page.dart';

class CameraFlow extends StatefulWidget {
  // 1
  final VoidCallback shouldLogOut;

  CameraFlow({Key key, this.shouldLogOut}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CameraFlowState();
}

class _CameraFlowState extends State<CameraFlow> {
  CameraDescription _camera;
  bool _shouldShowCamera = false;
  StorageService _storageService;

  List<MaterialPage> get _pages {
    return [
      MaterialPage(
        child: GalleryPage(
          imageUrlsController: _storageService.imageUrlsController,
          shouldLogOut: widget.shouldLogOut,
          shouldShowCamera: () => _toggleCameraOpen(true),
        ),
      ),
      if (_shouldShowCamera)
        MaterialPage(
          child: CameraPage(
            camera: _camera,
            didProvideImagePath: (imagePath) {
              this._toggleCameraOpen(false);
              this._storageService.uploadImageAtPath(imagePath);
            },
          ),
        )
    ];
  }

  @override
  void initState() {
    super.initState();
    _getCamera();
    _storageService = StorageService();
    _storageService.getImages();
  }

  @override
  Widget build(BuildContext context) {
    // 4
    return Navigator(
      pages: _pages,
      onPopPage: (route, result) => route.didPop(result),
    );
  }

  void _toggleCameraOpen(bool isOpen) {
    setState(() {
      this._shouldShowCamera = isOpen;
    });
  }

  void _getCamera() async {
    final camerasList = await availableCameras();
    setState(() {
      final firstCamera = camerasList.first;
      this._camera = firstCamera;
    });
  }
}
