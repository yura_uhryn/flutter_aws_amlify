import 'dart:async';

import 'package:aws_amlify_flutter/services/analytics.dart';
import 'package:aws_amlify_flutter/services/analytics_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class GalleryPage extends StatelessWidget {
  final StreamController<List<String>> imageUrlsController;
  final VoidCallback shouldLogOut;
  final VoidCallback shouldShowCamera;

  GalleryPage({
    Key key,
    this.imageUrlsController,
    this.shouldLogOut,
    this.shouldShowCamera,
  }) : super(key: key) {
    AnalyticsService.log(ViewGalleryEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gallery'),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: GestureDetector(
              child: Icon(Icons.logout),
              onTap: shouldLogOut,
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        onPressed: shouldShowCamera,
      ),
      body: Container(child: _galleryGrid()),
    );
  }

  Widget _galleryGrid() => StreamBuilder(
      // 1
      stream: imageUrlsController.stream,
      builder: (context, snapshot) {
        // 2
        if (snapshot.hasData) {
          // 3
          if (snapshot.data.length != 0) {
            return GridView.builder(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              // 4
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) => CachedNetworkImage(
                imageUrl: snapshot.data[index],
                fit: BoxFit.cover,
                placeholder: (context, url) => Container(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                ),
              ),
            );
          } else {
            // 5
            return Center(child: Text('No images to display.'));
          }
        } else {
          // 6
          return Center(child: CircularProgressIndicator());
        }
      },
    );
}
