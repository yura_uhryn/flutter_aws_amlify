const amplifyconfig = ''' {
    "UserAgent": "aws-amplify-cli/2.0",
    "Version": "1.0",
    "auth": {
        "plugins": {
            "awsCognitoAuthPlugin": {
                "UserAgent": "aws-amplify-cli/0.1.0",
                "Version": "0.1.0",
                "IdentityManager": {
                    "Default": {}
                },
                "CredentialsProvider": {
                    "CognitoIdentity": {
                        "Default": {
                            "PoolId": "eu-west-1:7cf9aacf-1ea0-484a-90e3-33ab3117dce7",
                            "Region": "eu-west-1"
                        }
                    }
                },
                "CognitoUserPool": {
                    "Default": {
                        "PoolId": "eu-west-1_MyIL4J5TE",
                        "AppClientId": "791gf167gomri7ha044gujpjc0",
                        "AppClientSecret": "fh5t989amjegks1m3asehe68n6gre7kmtuerge9hqd5anrs8v9d",
                        "Region": "eu-west-1"
                    }
                },
                "Auth": {
                    "Default": {
                        "authenticationFlowType": "USER_SRP_AUTH"
                    }
                },
                "PinpointAnalytics": {
                    "Default": {
                        "AppId": "3acfad6a2583434288b66c97db2bbf1c",
                        "Region": "eu-west-1"
                    }
                },
                "PinpointTargeting": {
                    "Default": {
                        "Region": "eu-west-1"
                    }
                },
                "S3TransferUtility": {
                    "Default": {
                        "Bucket": "awsamlifyfluttercb7a4bc8917f401d9469ac96bd37874122701-dev",
                        "Region": "eu-west-1"
                    }
                }
            }
        }
    },
    "analytics": {
        "plugins": {
            "awsPinpointAnalyticsPlugin": {
                "pinpointAnalytics": {
                    "appId": "3acfad6a2583434288b66c97db2bbf1c",
                    "region": "eu-west-1"
                },
                "pinpointTargeting": {
                    "region": "eu-west-1"
                }
            }
        }
    },
    "storage": {
        "plugins": {
            "awsS3StoragePlugin": {
                "bucket": "awsamlifyfluttercb7a4bc8917f401d9469ac96bd37874122701-dev",
                "region": "eu-west-1",
                "defaultAccessLevel": "guest"
            }
        }
    }
}''';