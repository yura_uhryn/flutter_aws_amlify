import 'dart:async';
import 'dart:io';
import 'package:amplify_core/amplify_core.dart';
import 'package:amplify_flutter/amplify.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';

class StorageService {
  // 1
  final imageUrlsController = StreamController<List<String>>();

  void getImages() async {
    try {
      final listOptions = S3ListOptions(accessLevel: StorageAccessLevel.guest);
      final result = await Amplify.Storage.list(options: listOptions);

      final getUrlOptions =
          GetUrlOptions(accessLevel: StorageAccessLevel.private);
      final List<String> imageUrls = await Future.wait(
        result.items.map(
          (item) async {
            final urlResult = await Amplify.Storage.getUrl(
                key: item.key, options: getUrlOptions);
            return urlResult.url;
          },
        ),
      );

      imageUrlsController.add(imageUrls);
    } catch (e) {
      print('Storage List error - $e');
    }
  }

  void uploadImageAtPath(String imagePath) async {
    final imageFile = File(imagePath);
    // 2
    final imageKey = '${DateTime.now().millisecondsSinceEpoch}.jpg';

    try {
      Map<String, String> metadata = <String, String>{};
      metadata['name'] = 'filename';
      metadata['desc'] = 'A test file';
      // 3
      final options = S3UploadFileOptions(
        accessLevel: StorageAccessLevel.private,
        metadata: metadata,
      );

      // 4
      await Amplify.Storage.uploadFile(
        local: imageFile,
        key: imageKey,
        options: options,
      );

      // 5
      getImages();
    } catch (e) {
      print('upload error - $e');
    }
  }
}
